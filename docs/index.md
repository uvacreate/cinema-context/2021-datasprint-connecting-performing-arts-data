# Datasprint: Connecting Performing Arts Data

![](images/create-logo-footer.png)

**Date**: 18 March 2021  
**Time**: 15:00-17:00  
**Location**: zoom  
**Please register here**: https://forms.gle/UgaM5Lqnkhe2XPc87

Slack channel: https://ccdatasprint.slack.com/archives/C01PGTK9DCL 

Zoom link: https://uva-live.zoom.us/j/82388598932

## Introduction

Over the last two decades, a growing number of databases have been created that record historical information on the production, distribution and reception of theatre, cinema and other instances of the performing arts. In order to stimulate the careful documentation and conservation of such datasets, the CREATE team has edited a special issue of data papers in the [Research Data Journal for the Humanities and Social Sciences](https://brill.com/view/journals/rdj/5/2/rdj.5.issue-2.xml). The launch of this special issue forms the occasion for this datasprint.

Data papers are academic texts that describe the origin and nature of a specific dataset, facilitating subsequent reuse. The nine data papers in this collection cover a range of historical datasets in the domain of the performing arts: music, theatre and cinema. Goal of the meeting is to explore the connections between those wide-ranging datasets, across national, temporal and disciplinary boundaries. An overview of the featured datasets can be [found here](http://www.shorturl.at/agBJV).

Below, we suggest some cases to explore connections between the datasets, but we encourage you to bring your own ideas and/or data. The meeting will be set up as follows: we start at 15:00 with a brief opening session where we take stock of the interests of the participants & form teams. Subsequently, each team works on its project and we report back at around 16:30 where the teams can present their work and discuss results.

## Use cases

### Case 1: Tracing individual works/creators
One way to make connections across these widely diverging data sets can be by following specific works or creators through the various datasets, focusing on works or makers that (re)occur throughout longer periods of time, and in various forms (musical, theatrical and cinematic performances/adaptations).
Beyond the datasets featured here, there is a wealth of online historical databases available on the topic, where one can go even further in tracing the works or creators in question. In 2018 we created [an inventory of online databases on European performing arts](http://www.shorturl.at/cuSUV) that can be of help (note that the overview has not been updated since).

### Case 2: Venue locations
Another way to make connections is through comparing and juxtaposing locations of theatre and cinema venues. A team of the Amsterdam University Library Theatre collection and UvA Theatre studies are currently working on such an initiative that aims at putting all Dutch cinema and theater venues on a historical map.

### Case 3: Linked open data
Several of the featured data sets are also available in RDF format ([Onstage](https://data.create.humanities.uva.nl/id/onstage/) and [Cinema Context](https://data.create.humanities.uva.nl/id/cinemacontext/)). Use the data sprint as an occasion to convert other featured data sets into RDF (e.g., using the [Clariah tool COW](http://cattle.datalegend.net/)). Or explore new connections with the existing Linked data sets, as has been put into practice in previous [Cinema Context datasprints](https://uvacreate.gitlab.io/cinema-context/cinema-context-rdf/events/datasprint20201008/), for instance by [linking to film posters in online heritage collections](https://uvacreate.gitlab.io/cinema-context/cinema-context-rdf/events/hackalod2020/). For example: can we compare Dutch film posters to their Flemish equivalents in the Antwerp Zoo data set?

### Case 4: BYO
Bring your own performing arts dataset or ideas and use the datasprint as an opportunity to explore connections, follow your interests and meet up with colleagues.
